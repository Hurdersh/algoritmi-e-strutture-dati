import fibonacci.*;
public class Main{
  public static void main(String[] args){
    FibonacciInterface f = new FibonacciArray();
    System.out.println(f.fibonacciSolve(Integer.parseInt(args[0])));
  }
}
