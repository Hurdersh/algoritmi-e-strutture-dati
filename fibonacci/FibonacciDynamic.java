package fibonacci;
public class FibonacciDynamic implements FibonacciInterface{
  public int fibonacciSolve(int n){
    int a = 1;
    int b = 1;
    int temp;
    for(int i=2;i<n;i++){
      temp = b;
      b = a + b;
      a = temp;
    } 
    return b;
  }
}

/*
 *Correttezza: Da definizione
 *Complessita' :
 *  -Memoria: utilizzo ben 3 locazioni di memoria  quindi theta(1)
 *  -Istuzioni: eseguo theta(n) istruzioni
 */
