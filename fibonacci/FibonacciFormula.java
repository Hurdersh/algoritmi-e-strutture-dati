package fibonacci;
import java.lang.Math;
public class FibonacciFormula implements FibonacciInterface{
  public int fibonacciSolve(int n){
   return (int) Math.round( (Math.pow(((1+Math.sqrt(5))/2), n) - Math.pow(((1-Math.sqrt(5))/2),n) )/Math.sqrt(5)); 
  }
}
/*
 *
 * Correttezza: Matematicamente Dimostrabile, usa la formula di binet.
 *              Utilizzando numeri irrazionali, l'approssimazione e' 
 *              relativa ad i bit da immagazzinare, a lungo andare per questioni
 *              architetturali(IEEE 754) non si avra' un dato corretto
 *
 * Efficienza: Questo Algoritmo e' il piu' efficiente possibile, 
 *  -memoria: Theta(1)
 *  -istruzioni: Theta(1)
 *
 * Note:
 *  -L'efficienza ideale in caso delle istruzioni e' in tempo costante, ma sia Math.sqrt che Math.pow 
 *  vengono eseguite in tempo (al meglio) logaritmico Omega(log n)
 */ 
