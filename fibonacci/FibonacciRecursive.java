package fibonacci;
public class FibonacciRecursive implements FibonacciInterface{
  public int fibonacciSolve(int n){
    if(n < 3)  
      return 1; //Fn volte 
    return fibonacciSolve(n-1) + fibonacciSolve(n-2); //Fn - 1 volte
  }
}
/*
 * Correttezza: Da definizione
 * Complessita':
 *  -Efficienza: Dato che il numero di foglie nell'albero della ricorsione e'
 *               proprio Fn ed il numero di Nodi interni di un albero Strettamente Binario e'
 *               esattamente (Numero di foglie - 1) = (Fn - 1) otteniamo una complessita'
 *               T(n) = Fn + 2*(Fn - 1) ~ 3Fn ~ phi^n ~ 1.618^n
 *  -Memoria:    Ogni volta che utilizzo la ricorsione e' come se tenessi in stallo le variabili
 *               che quindi saranno quante le chiamate ricorsive ~ 1.618^n
 */
