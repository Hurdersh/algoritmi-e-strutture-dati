package fibonacci;
public class FibonacciArray implements FibonacciInterface{
  public int fibonacciSolve(int n){
    int[] array = new int[n];
    array[0] = 1;
    array[1] = 1;
    for(int i = 2;i<n;i++){
      array[i] = array[i-1] + array[i-2];
    }
    return array[n-1];
  }
}

/*
 *  Correttezza: da definizione
 *  Complessita':
 *    -memoria: utilizzo un array,per tanto utilizzo n locazioni di memoria
 *    -istruzioni: eseguo sempre theta(n) istruzioni
 *  Analisi molto semplice di un algoritmo lineare
 */
